# frozen_string_literal: true

RSpec.describe CoinMarketPro do
  describe '.new' do
    it { expect(described_class.new(api_key: '123')).to be_a(CoinMarketPro::Client::Base) }
  end

  describe 'version' do
    it 'has a version number' do
      expect(CoinMarketPro::VERSION).to eq('0.1.2')
    end
  end
end
