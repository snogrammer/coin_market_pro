# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'coin_market_pro/version'

Gem::Specification.new do |spec|
  spec.name          = 'coin_market_pro'
  spec.version       = CoinMarketPro::VERSION
  spec.authors       = ['snogrammer']

  spec.summary       = 'CoinMarketCap Pro v1 Api Ruby wrapper'
  spec.homepage      = 'https://gitlab.com/snogrammer/coin_market_pro'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '~> 5.1'
  spec.add_dependency 'multi_json',    '~> 1.13'
  spec.add_dependency 'rest-client',   '~> 2.0'

  spec.add_development_dependency 'bundler',    '~> 1.16'
  spec.add_development_dependency 'pry',        '~> 0.11'
  spec.add_development_dependency 'rake',       '~> 12.3'
  spec.add_development_dependency 'rspec',      '~> 3.8'
  spec.add_development_dependency 'simplecov',  '~> 0.16'
  spec.add_development_dependency 'webmock',    '~> 3.4'
end
