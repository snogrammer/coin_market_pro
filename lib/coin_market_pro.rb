# frozen_string_literal: true

require 'coin_market_pro/version'
require_relative 'coin_market_pro/client/base'

module CoinMarketPro
  module_function

  # @param args [Hash]
  # @param args [String] :api_key Required. Defaults to CMC_PRO_API_KEY
  # @param args [Boolean] :sandbox Defaults to false
  # @param args [Logger] :logger Defaults to Logger.new(STDOUT)
  # @param args [Integer] :timeout Defaults to 10
  def new(**args)
    Client::Base.new(args)
  end
end
